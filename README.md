# FIREBLAZE AI SCHOOL

## Website design for fireblazeaischool.in

## Changes and Features added to the original project:

### New Features added:
 1. Brand new responsive nav-bar with login and signup button
 2. Picture carousel in gallery section
 3. go-to-top button at the bottom right corner of the screen allowing user to go directly to the top of the webpage
 4. Google map embedded in the address section allowing user to navigate to the address directly using google map
 5. More social buttons added, i.e Titter.

 ### Visual Changes:

 1. The color schem of the website changed to red resembling the fireblaze logo.
 2. Material design card look given to course logo in course section.
 3. Tweaked and fixed Header background to give a parallax effect when scrolling the page.
 4. Moved Alumnus description below their photos.
 5. Tweaked padding and margin of various elements to make it look perfect on any screen size device.

 ### Bug fixes:

 1. Fixed horizontal body overflow issue.
 2. Fixed Typo in header paragraph.
 3. Fixed navbar to make it responsive.
 4. Add alt description to all the picture for better webpage accessibitily.
 5. Fixed some typo and uneffective styling in main.css file.

 Thanks.
 Shubham Prakash
 (shubham.prakash2308@gmail.com)
 LinkedIn: https://www.linkedin.com/in/ishubhamprakash/
 GitHub: https://github.com/i-shubhamprakash